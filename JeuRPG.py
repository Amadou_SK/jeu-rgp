import random

nomP1 = input("Insérez un nom : ")
nomP2 = input("Insérez le nom de votre adversaire : ")

class Personnage:
    def __init__(self, nom, PV, attack):
        self.nom = nom
        self.PV = PV
        self.attack = attack

    def pointV(self):
        print(f"{self.PV} sont les points de vie de {self.nom}.")

    def attackPerso(self):
        print(f"{self.attack} sont les points d'attaque de {self.nom}.")

    def combat(self, adversaire):
        gagnant = None

        while self.PV > 0 and adversaire.PV > 0:
            AttPerso1 = self.attack
            AttPerso2 = adversaire.attack

            self.PV -= AttPerso2
            adversaire.PV -= AttPerso1

            print(f"{self.nom} : {self.PV} PV | {adversaire.nom} : {adversaire.PV} PV")
            print("                                    ")
            input("Entrez pour continuer")

            if self.PV <= 0:
                gagnant = adversaire
            elif adversaire.PV <= 0:
                gagnant = self
            elif self.PV == adversaire.PV:
                print("egalité personne a gagné")

        print(f"{gagnant.nom} a gagné le combat!")

        return gagnant

class Humain(Personnage):
    def __init__(self, nom):
        super().__init__(nom, PV=random.randint(1000,1500), attack=random.randint(50, 100))

    def nomPerso1(self):
        print(f"{self.nom} est bon.")


class Monster(Personnage):

    def __init__(self, nom):
        super().__init__(nom, PV=random.randint(1000,1500), attack=random.randint(50, 100))

    def nomPerso2(self):
        print(f"{self.nom} est méchant.")

# Création des instances
humain = Humain(nomP1)
monster = Monster(nomP2)


humain.attackPerso()
print("                                  ")
humain.pointV()
#humain.nomPerso1()

print("                                  ")
print("############## VS ################")
print("                                  ")

monster.attackPerso()
print("                                  ")
monster.pointV()
#monster.nomPerso2()


gagnant = humain.combat(monster)













    
